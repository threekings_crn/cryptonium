var Token = artifacts.require("./Cryptonium.sol");

var tokenContract;

module.exports = function(deployer) {
    var admin = "0x0bc807642fcdCbBcEa4d17c19C67D214192aa939"; 
    var totalTokenAmount = 1 * 100000000000 * 1000000000000000000;
    return Token.new(admin, totalTokenAmount).then(function(result) {
        tokenContract = result;
    });
};
