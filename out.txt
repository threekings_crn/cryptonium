
> NPLC@1.0.0 coverage /Users/lee/cryptonium/CRN
> scripts/coverage.sh

Starting our own ganache instance
Generating coverage environment
Running: node --max-old-space-size=4096 ../node_modules/.bin/truffle compile --network coverage
(this can take a few seconds)...
Compiling ./contracts/Administratable.sol...
Compiling ./contracts/Cryptonium.sol...
Compiling ./contracts/Freezable.sol...
Compiling ./contracts/Migrations.sol...
Compiling openzeppelin-solidity/contracts/math/SafeMath.sol...
Compiling openzeppelin-solidity/contracts/ownership/Ownable.sol...
Compiling openzeppelin-solidity/contracts/token/ERC20/ERC20.sol...
Compiling openzeppelin-solidity/contracts/token/ERC20/ERC20Burnable.sol...
Compiling openzeppelin-solidity/contracts/token/ERC20/IERC20.sol...
Writing artifacts to ./build/contracts

Instrumenting  ./coverageEnv/contracts/Administratable.sol
Instrumenting  ./coverageEnv/contracts/Cryptonium.sol
Instrumenting  ./coverageEnv/contracts/Freezable.sol
Skipping instrumentation of  ./coverageEnv/contracts/Migrations.sol
Running: node --max-old-space-size=4096 ../node_modules/.bin/truffle compile --network coverage
(this can take a few seconds)...
Compiling ./contracts/Administratable.sol...
Compiling ./contracts/Cryptonium.sol...
Compiling ./contracts/Freezable.sol...
Compiling ./contracts/Migrations.sol...
Compiling openzeppelin-solidity/contracts/math/SafeMath.sol...
Compiling openzeppelin-solidity/contracts/ownership/Ownable.sol...
Compiling openzeppelin-solidity/contracts/token/ERC20/ERC20.sol...
Compiling openzeppelin-solidity/contracts/token/ERC20/ERC20Burnable.sol...
Compiling openzeppelin-solidity/contracts/token/ERC20/IERC20.sol...

Compilation warnings encountered:

openzeppelin-solidity/contracts/math/SafeMath.sol:13:3: Warning: Function state mutability can be restricted to pure
  function mul(uint256 a, uint256 b) internal  returns (uint256) {
  ^ (Relevant source part starts here and spans across multiple lines).
,openzeppelin-solidity/contracts/math/SafeMath.sol:30:3: Warning: Function state mutability can be restricted to pure
  function div(uint256 a, uint256 b) internal  returns (uint256) {
  ^ (Relevant source part starts here and spans across multiple lines).
,openzeppelin-solidity/contracts/math/SafeMath.sol:41:3: Warning: Function state mutability can be restricted to pure
  function sub(uint256 a, uint256 b) internal  returns (uint256) {
  ^ (Relevant source part starts here and spans across multiple lines).
,openzeppelin-solidity/contracts/math/SafeMath.sol:51:3: Warning: Function state mutability can be restricted to pure
  function add(uint256 a, uint256 b) internal  returns (uint256) {
  ^ (Relevant source part starts here and spans across multiple lines).
,openzeppelin-solidity/contracts/math/SafeMath.sol:62:3: Warning: Function state mutability can be restricted to pure
  function mod(uint256 a, uint256 b) internal  returns (uint256) {
  ^ (Relevant source part starts here and spans across multiple lines).
,openzeppelin-solidity/contracts/ownership/Ownable.sol:31:3: Warning: Function state mutability can be restricted to view
  function owner() public  returns(address) {
  ^ (Relevant source part starts here and spans across multiple lines).
,openzeppelin-solidity/contracts/ownership/Ownable.sol:46:3: Warning: Function state mutability can be restricted to view
  function isOwner() public  returns(bool) {
  ^ (Relevant source part starts here and spans across multiple lines).

Writing artifacts to ./build/contracts

Running: node --max-old-space-size=4096 ../node_modules/.bin/truffle test --network coverage
(this can take a few seconds)...
Using network 'coverage'.

[0m[0m
[0m  Contract: Administratable[0m
[0m    addSuperAdmin[0m
    [32m  ✓[0m[90m contract owner can add a super Admin[0m ([32m54892 gas[0m)
    [32m  ✓[0m[90m a super admin can not add another super admin[0m ([32m23292 gas[0m)
    [32m  ✓[0m[90m a non super admin cannot add another super admin[0m ([32m23292 gas[0m)
    [32m  ✓[0m[90m cannot add a super admin that is already a super admin[0m ([32m85728 gas[0m)
[0m    removeSuperAdmin[0m
    [32m  ✓[0m[90m contract owner can remove a super Admin[0m ([32m79671 gas[0m)
    [32m  ✓[0m[90m a super admin can not remove another super admin[0m ([32m221863 gas[0m)
    [32m  ✓[0m[90m a non super admin cannot add another super admin[0m ([32m23292 gas[0m)
    [32m  ✓[0m[90m cannot add a super admin that is already a super admin[0m ([32m85728 gas[0m)

[0m  Contract: Cryptonium[0m
[0m    constructor[0m
    [32m  ✓[0m[90m validate token minting[0m
[0m    transfer[0m
    [32m  ✓[0m[90m simple transfer case should succeed and change balance[0m ([32m64611 gas[0m)
    [32m  ✓[0m[90m transferFrom case should succeed and change balance[0m ([32m179895 gas[0m)
    [32m  ✓[0m[90m transfer fails when token is frozen[0m ([32m83035 gas[0m)
    [32m  ✓[0m[90m transfer fails if sender is frozen[0m ([32m92595 gas[0m)
    [32m  ✓[0m[90m transfer fails if destination is frozen[0m ([32m92929 gas[0m)
    [32m  ✓[0m[90m transfer fails if destination is not a valid address[0m ([32m24298 gas[0m)
    [32m  ✓[0m[90m when the spender is the frozen account[0m ([32m91827 gas[0m)
    [32m  ✓[0m[90m when the spender is the frozen token[0m ([32m114327 gas[0m)
    [32m  ✓[0m[90m when the spender is the frozen account(message sender)[0m ([32m126636 gas[0m)
    [32m  ✓[0m[90m increase allowance[0m ([32m102022 gas[0m)
    [32m  ✓[0m[90m decrease allowance[0m ([32m102286 gas[0m)
    [32m  ✓[0m[90m change owner and add super admin[0m ([32m134592 gas[0m)

[0m  Contract: Freezable[0m
[0m    freezeAccount[0m
    [32m  ✓[0m[90m contract owner can freeze token[0m ([32m78926 gas[0m)
    [32m  ✓[0m[90m non owner can not freeze token[0m ([32m24421 gas[0m)
    [32m  ✓[0m[90m contract owner can add a frozen account[0m ([32m58574 gas[0m)
    [32m  ✓[0m[90m a non super admin can not freeze another account[0m ([32m25994 gas[0m)
    [32m  ✓[0m[90m cannot add a frozen account that is already a frozen account[0m ([32m92794 gas[0m)

·--------------------------------------------------------------------------------|-----------------------------------·
|                                      [32m[1mGas[22m[39m                                       ·  [90mBlock limit: 17592186044415 gas[39m  │
··········································|······································|····································
|  [32m[1mMethods[22m[39m                                ·              [90m5 gwei/gas[39m              ·         [31m137223.66 krw/eth[39m         │
····················|·····················|············|············|············|··················|·················
|  [1mContract[22m         ·  [1mMethod[22m             ·  [32mMin[39m       ·  [32mMax[39m       ·  [32mAvg[39m       ·  [1m# calls[22m         ·  [1mkrw (avg)[22m     │
····················|·····················|············|············|············|··················|·················
|  [90mAdministratable[39m  ·  addSuperAdmin      ·         -  ·         -  ·     54892  ·               [90m6[39m  ·         [32m37.66[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mAdministratable[39m  ·  removeSuperAdmin   ·         -  ·         -  ·     24779  ·               [90m1[39m  ·         [32m17.00[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  addSuperAdmin      ·         -  ·         -  ·     55222  ·               [90m1[39m  ·         [32m37.89[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  approve            ·     [36m58031[39m  ·     [31m58095[39m  ·     58052  ·               [90m3[39m  ·         [32m39.83[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  decreaseAllowance  ·         -  ·         -  ·     44255  ·               [90m1[39m  ·         [32m30.36[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  freezeAccount      ·     [36m58860[39m  ·     [31m59190[39m  ·     59025  ·               [90m4[39m  ·         [32m40.50[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  freezeToken        ·     [36m51695[39m  ·     [31m52025[39m  ·     51860  ·               [90m2[39m  ·         [32m35.58[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  increaseAllowance  ·         -  ·         -  ·     43991  ·               [90m1[39m  ·         [32m30.18[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  removeSuperAdmin   ·         -  ·         -  ·     24933  ·               [90m1[39m  ·         [32m17.11[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  transfer           ·         -  ·         -  ·     64611  ·               [90m2[39m  ·         [32m44.33[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  transferFrom       ·         -  ·         -  ·     57189  ·               [90m1[39m  ·         [32m39.24[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mCryptonium[39m       ·  transferOwnership  ·         -  ·         -  ·     30815  ·               [90m1[39m  ·         [32m21.14[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mFreezable[39m        ·  freezeAccount      ·         -  ·         -  ·     58574  ·               [90m2[39m  ·         [32m40.19[39m  │
····················|·····················|············|············|············|··················|·················
|  [90mFreezable[39m        ·  freezeToken        ·         -  ·         -  ·     51519  ·               [90m1[39m  ·         [32m35.35[39m  │
····················|·····················|············|············|············|··················|·················
|  [32m[1mDeployments[22m[39m                            ·                                      ·  [1m% of limit[22m      ·                │
··········································|············|············|············|··················|·················
|  Administratable                        ·         -  ·         -  ·   2132614  ·             [90m0 %[39m  ·       [32m1463.23[39m  │
··········································|············|············|············|··················|·················
|  Cryptonium                             ·  [36m10950532[39m  ·  [31m10950596[39m  ·  10950543  ·             [90m0 %[39m  ·       [32m7513.37[39m  │
··········································|············|············|············|··················|·················
|  Freezable                              ·         -  ·         -  ·   4037600  ·             [90m0 %[39m  ·       [32m2770.27[39m  │
·-----------------------------------------|------------|------------|----------------------|----------|----------|----------|----------|----------------|
File                  |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
----------------------|----------|----------|----------|----------|----------------|
 contracts/           |      100 |      100 |      100 |      100 |                |
  Administratable.sol |      100 |      100 |      100 |      100 |                |
  Cryptonium.sol      |      100 |      100 |      100 |      100 |                |
  Freezable.sol       |      100 |      100 |      100 |      100 |                |
----------------------|----------|----------|----------|----------|----------------|
All files             |      100 |      100 |      100 |      100 |                |
----------------------|----------|----------|----------|----------|----------------|

Istanbul coverage reports generated
Cleaning up...
Done.
